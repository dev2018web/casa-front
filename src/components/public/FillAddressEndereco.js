import React, { Component } from 'react';
import { Container } from './Home';
import TitleBox from './shared/BoxesTitles';
import { FormInformations } from './shared/Forms';
import {Row, Col} from 'react-bootstrap';
import { restricted } from './configs/restrictedArea';

class FillAddressEndereco extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
      restricted(this);
        
        return (
            <Container>
                <TitleBox text={"Informações para empréstimo"}/>
                
               
                <FormInformations classe={this}/>
            </Container>  
        );
    }
}
 
export default FillAddressEndereco;