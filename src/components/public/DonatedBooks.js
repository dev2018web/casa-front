import React from 'react';
import axios from 'axios';
import TitleBox, {SubtitleBox} from './shared/BoxesTitles';
import { BoxSubMenu } from './shared/Header';
import { Container, ContainerFluid, SubContainer, HorizontalBoxes } from './shared/Product';
import { restricted } from './configs/restrictedArea';
import { Row, Col } from 'react-bootstrap';


class DonatedBooks extends React.Component {
    constructor(props){
      super(props);
      this.state = props;

      this.state = {
        book: [],
        books: [],
        livro_id: this.props.match.params.livro_id
      }

      this.getBook();
      this.getBooks();
    }

    getBooks = () => {
      axios.get(`https://www.googleapis.com/books/v1/volumes?q=filosofia`)
      .then(res => {
        const { items } = res.data;

        this.setState({ books: items });
      })
    }


    getBook = () => {
      const {livro_id} = this.state;
      
      axios.get(`https://www.googleapis.com/books/v1/volumes/${livro_id}`)
      .then(res => {
        const { volumeInfo } = res.data;

        this.setState({ book: volumeInfo });
      })
    }

    render() {
      const { book, books } = this.state;
      restricted(this);

      return (
        <ContainerFluid>
          <Row className="row-no-padding">
            <Col md={3}><BoxSubMenu/></Col>
              <Col md={5}>
               <SubContainer>
                  <SubtitleBox text={"Livros Doados"}/>

                  <HorizontalBoxes data={books}/>
                </SubContainer>
              </Col>
          </Row>          
        </ContainerFluid>
      );
    }
  }


export default DonatedBooks;