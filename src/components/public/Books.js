import React from 'react';
import { Slider } from './shared/Header';
import Product, {BookDetailed} from './shared/Product';
import axios from 'axios';
import styled from 'styled-components';
import TitleBox, {SubtitleBox} from './shared/BoxesTitles';
import { BoxSubMenu } from './shared/Header';
import { Container, ContainerFluid, SubContainer } from './shared/Product';
import { restricted } from './configs/restrictedArea';
import { Row, Col } from 'react-bootstrap';


class Books extends React.Component {
    constructor(props){
      super(props);
      this.state = props;

      this.state = {
        book: [],
        books: [],
        livro_id: this.props.match.params.livro_id
      }

      this.getBook();
      this.getBooks();
    }

    getBooks = () => {
      axios.get(`https://www.googleapis.com/books/v1/volumes?q=2`)
      .then(res => {
        const { items } = res.data;

        this.setState({ books: items });
      })
    }


    getBook = () => {
      const {livro_id} = this.state;
      
      axios.get(`https://www.googleapis.com/books/v1/volumes/${livro_id}`)
      .then(res => {
        const { volumeInfo } = res.data;

        this.setState({ book: volumeInfo });
      })
    }

    render() {
      const { book, books } = this.state;
      restricted(this);

      return (
        <ContainerFluid>
            <Row className="row-no-padding">
            <Col sm={12} md={5} lg={3}><BoxSubMenu/></Col>
              <Col sm={12} md={7} lg={9}>
                <SubContainer>
                  <SubtitleBox text={"Livros"}/>
                  <Product data={{books}}/>
                </SubContainer>
              </Col>
          </Row>
        </ContainerFluid>
      );
    }
  }


export default Books;