import React from 'react';
import { LoremIpsum, Avatar } from 'react-lorem-ipsum';
import styled from 'styled-components';
import {Input} from './shared/Forms';
import { ButtonLogin, TextButtonLogin } from './shared/Buttons';
import { Alert, Button } from 'react-bootstrap';

const bgSubBox = "#CDEEE4";
const titleColor = "#47525E";

const RelativeBox = styled.div`
    position: relative;
    z-index: 3;
    margin-top: 70px;
`;

const BoxSubBox = styled(RelativeBox)`
    background-color: ${bgSubBox};
    margin: 10px;
    border-radius: 20px;
    min-height: 700px;
    padding: 50px;
    
`;

const TitleSubBox = styled.h1`
    font-size: 40px;
    font-weight: bold;
    font-family: Lato, Arial;
    color: ${titleColor};
    margin-bottom: 25px;
`;

class Login extends React.Component {
    state = {
        login: {
            email: '',
            password: ''
        },

        registerData: {
            email: '',
            password: '',
            conf_password: '',
        },
        message: '',
        loginMessagevisible: false,
        registerMessagevisible: false,
    }

    handleInputLoginChange = (event) => {
        const {name} = event.target;
        const {value} = event.target;

        const {login} = this.state;

        this.setState({login: {...login, [name]: value}})
    }

    handleInputRegisterChange = (event) => {
        const {name} = event.target;
        const {value} = event.target;

        const {registerData} = this.state;

        this.setState({registerData: {...registerData, [name]: value}})
    }

    handleLogin = () => {
        const { email, password } = this.state.login;

        if(email && password){
                if (email == "pr838908@gmail.com" && password == "123456") {
                    sessionStorage.setItem("token", "1234567890");
                    this.closeMessageBoxes();
                    window.location.href = "/home";

                }else{
                    this.closeMessageBoxes();
                    this.openLoginMessage("Login incorreto!");
                }
        }else{
            this.closeMessageBoxes();
            this.openLoginMessage("Você precisa digitar e-mail e senha");
        }
    }

    handleRegister = () => {
        const { email, password, conf_password } = this.state.registerData;

        if(email && password && conf_password){
            if(password != conf_password){
                this.closeMessageBoxes();
                this.openRegisterMessage("Senha e confirmação de senha estão diferentes.");
            }else{
                this.closeMessageBoxes();
                sessionStorage.setItem("token", "1234567890");
                window.location.href = "/home";
            }
        }else{
            this.closeMessageBoxes();
            this.openRegisterMessage("Você precisa preencher todos os campos");
        }
    }

    closeMessageBoxes = () => {
        this.setState({loginMessagevisible: false, registerMessagevisible: false});
    }

    openLoginMessage = (message) => {
        this.setState({message, loginMessagevisible: true})
    }

    openRegisterMessage = (message) => {
        this.setState({message, registerMessagevisible: true})
    }

    render(){
        const { message, loginMessagevisible, registerMessagevisible } = this.state;

        return(
            <RelativeBox className="container login">
                <div className="row">

                    <BoxSubBox className="col"> 
                        <Alert show={loginMessagevisible} variant="danger">
                            <p>{message}</p>
                        </Alert>
                        <TitleSubBox>Login</TitleSubBox>

                        <form>
                            <div className="form-group">
                                <Input type="email" className="form-control" name="email"  aria-describedby="emailHelp" placeholder="Digite seu e-mail" onChange={this.handleInputLoginChange}/>
                            </div>
                            <div className="form-group">
                                <Input type="password" className="form-control" name="password" placeholder="Senha" onChange={this.handleInputLoginChange} />
                            </div>
                            <ButtonLogin className="text-white" onClick={this.handleLogin}><TextButtonLogin>Login</TextButtonLogin></ButtonLogin>
                        </form>
                    </BoxSubBox>
                    
                    <BoxSubBox className="col">
                    <Alert show={registerMessagevisible} variant="danger">
                        <p>{message}</p>
                    </Alert> 
                    <TitleSubBox>Cadastro</TitleSubBox>

                    <form>
                        <div className="form-group">
                            <Input type="email" className="form-control" name="email" placeholder="Digite seu e-mail"  onChange={this.handleInputRegisterChange}/>
                        </div>
                        <div className="form-group">
                            <Input type="password" className="form-control" name="password" placeholder="Senha" onChange={this.handleInputRegisterChange} />
                        </div>
                        <div className="form-group">
                            <Input type="password" className="form-control" name="conf_password"  placeholder="Repita a senha" onChange={this.handleInputRegisterChange}/>
                        </div>
                        <ButtonLogin className="text-white" onClick={this.handleRegister}><TextButtonLogin>Login</TextButtonLogin></ButtonLogin>
                    </form>
                    </BoxSubBox>
                    <BoxSubBox className="col"> 
                        <TitleSubBox>Informações</TitleSubBox>

                        
                    </BoxSubBox>
                </div>
            </RelativeBox>
        );
    }
}

export default Login;