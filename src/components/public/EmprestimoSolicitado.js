import React, { Component } from 'react';
import { Container } from './Home';
import styled from 'styled-components';
import { LoremIpsum } from 'react-lorem-ipsum/dist/lorem-ipsum';
import { restricted } from './configs/restrictedArea';

const TitlePage = styled.h1`
    font-size: 40px;
    font-family: Lato, Arial, Helvetica, sans-serif;
    font-weight: bold;
`;
const SubTitlePage = styled(TitlePage)`
    font-size: 35px;
    margin-top: 20px;
`;

const SubBoxTitle = styled(TitlePage)`
    margin-bottom: 20px;
    font-size: 30px;
`;

const Box = styled.div`
    margin: 80px 0;
    position: relative;
    z-index: 2;

    &:before {
        content: '';
        display: block;
        position: absolute;
        top: 0;
        width: 100%;
        bottom: 0;
        transform: translate(10%, -15%);
        background: #13CE66;
        z-index: -1;
        height: 150px;
    }
`;

const SubBox = styled.div`
    margin-top: 80px;
    font-size: 20px;
    font-weight: bold; 
    font-family: Lato, Arial, Helvetica, sans-serif;
`;

class EmprestimoSolicitado extends Component {
    state = {  }
    render() { 
      restricted(this);
        
        return ( 
            <Container>
                <Box>
                    <TitlePage>Parabéns, Paulo Ricardo</TitlePage>
                    <SubTitlePage>Seu empréstimo foi realizado com sucesso!</SubTitlePage>


                    <SubBox>
                        <SubBoxTitle>E agora! O que eu faço?</SubBoxTitle>

                        <LoremIpsum/>
                    </SubBox>
                </Box>
            </Container>
         );
    }
}
 
export default EmprestimoSolicitado;