
export const restricted = (classe) => {
	if(!sessionStorage.getItem("token")){
		classe.props.history.push("/login");
	}
}