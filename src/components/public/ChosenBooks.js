import React from 'react';
import { Container } from './Home';
import { ChosedBooksBox, BooksChosenArea } from './shared/Product';
import { BtnProsseguirChosen, TextButton } from './shared/Buttons';
import axios from 'axios';
import styled from 'styled-components';
import TitleBox, {SubtitleBox} from './shared/BoxesTitles';
import { restricted } from './configs/restrictedArea';
import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default class ChosenBooks extends React.Component{
	state = {
		books: [],
		booksProduct: []
	}

	componentDidMount(){
		this.getBooks();
	}

	getBooks = () => {
      axios.get(`https://www.googleapis.com/books/v1/volumes?q=software`)
      .then(res => {
        const { items } = res.data;

        this.setState({ books: items, booksProduct: items });
      })
    }

	addBook = (book) => {
		const {books} = this.state;

		const newList = books;
		newList.push(book);

		console.log(books);

		this.setState({books: newList});
	}

	removeBook = (book_id) => {
		const {books} = this.state;

		const response = books.filter(function(book){
			return book.id != book_id;
		})

		this.setState({books: response});
	}

	render(){
		const {books, booksProduct} = this.state;
      	restricted(this);


		return (
			<Container>
                <SubtitleBox text={"Livros Escolhidos"}/>
					<Row>
						<Col>
							<ChosedBooksBox data={books}  deleteBook={this.removeBook}/>
						</Col>
						<Col>
							<Link to={'/solicitacao-emprestimo'}><BtnProsseguirChosen style={{marginTop: '20px'}} ><TextButton>Prosseguir</TextButton></BtnProsseguirChosen></Link>
						</Col>
					</Row>
	

                <SubtitleBox text={"Outras Recomendações"}/>
              	
				<BooksChosenArea data={{booksNotChosed: booksProduct, booksChosed: books,  setBookId: this.setBookId, addBook: this.addBook}}/>
			</Container>
		)
	}

}