import React from 'react';
import { Slider } from './shared/Header';
import Product, {BookDetailed} from './shared/Product';
import axios from 'axios';
import styled from 'styled-components';
import TitleBox, {SubtitleBox} from './shared/BoxesTitles';
import { restricted } from './configs/restrictedArea';

const Container = styled.div`
  max-width: 1280px;
  margin: auto;
  z-index: 2; 
  position: relative;
`;

class Home extends React.Component {
    constructor(props){
      super(props);
      this.state = props;

      this.state = {
        book: [],
        books: [],
        livro_id: this.props.match.params.livro_id
      }

      this.getBook();
      this.getBooks();
    }

    getBooks = () => {
      axios.get(`https://www.googleapis.com/books/v1/volumes?q=amor`)
      .then(res => {
        const { items } = res.data;

        this.setState({ books: items });
      })
    }


    getBook = async (id_book) => {
        var {livro_id} = this.state;
      
      
      await axios.get(`https://www.googleapis.com/books/v1/volumes/${livro_id}`)
      .then(res => {
        const { volumeInfo } = res.data;

        this.setState({ book: volumeInfo });
      })
    }

    setBookId = async () => {

      console.log(this.props.match)
      await this.setState({livro_id: this.props.match.params.livro_id})
      this.getBook();
    }

    render() {
      const { book, books } = this.state;
      restricted(this);

      return (
        <Container>

          <TitleBox text={"Livro Selecionado"}/>
          <BookDetailed data={book}/>


          <SubtitleBox text={"Livro Selecionado"}/>
          <Product data={{books, setBookId: this.setBookId}}/>
        </Container>
      );
    }
  }


export default Home;