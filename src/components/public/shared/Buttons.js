import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom'

const colorText = "#fff";
const colorBg = "#009CFF";
const colorBorder = "#009CFF";
const bgBtnLogin = "#0D1926";
const btnProsseguirEscolhabg = "#13CE66";
const borderButtonAddBook = "#AD015B";

export const Button = styled.a`
    border: 2px solid ${borderButtonAddBook};
    border-radius: 3px;
    text-align: center;
    height: 50px;
    display: flex;
    align-itens: center;
    justify-content: center;
    cursor: pointer;
    position: relative;
    z-index: 
`;

export const TextButton = styled.p`
    align-self: center;
    color: ${colorText};
    font-family: Lato;
    text-decoration: none;
`;

export const TextButtonAddBook = styled(TextButton)`
    color: ${borderButtonAddBook};
    text-transform: uppercase;
    font-size: 14px;
    font-weight: bold;
`;


export const TextButtonLogin = styled(TextButton)``;

export const ButtonProsseguir = styled(Button)`
    background-color: ${colorBg};
    font-size: 20px;
    font-weight: bold;
    text-transform: uppercase;
    width: 220px;
    min-height: 60px;
    max-width: 100%;
    margin-top: 10px;
    font-family: 'Lato', sans-serif;
`;

export const BtnProsseguirChosen = styled(ButtonProsseguir)`
    min-height: 80px;
    width: 60%;
    position: sticky;
    top: 10px;
    background-color: ${btnProsseguirEscolhabg};
    border: none;
`;

export const BtnProsseguirEndereco = styled(BtnProsseguirChosen)`
    position: relative;
    margin-bottom: 30px;
    align-self: flex-end;
`;

export const ButtonLogin = styled(Button)`
    background-color: ${bgBtnLogin};
    background: #0D1926;
    width: 50%;
    border: none;
`;