import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom'

const boxTitleBg = "#AD015B";
const Div = styled.div`
    min-height: 80px;
    background-color: ${boxTitleBg};
    border-radius: 8px;
    display: inline-block;
    margin-top: 10px;
    position: relative;
    font-family: 'grand_hotelregular';
    font-weight: bold;
    
    ::before{
        content: '';
        content: '';
        position: absolute;
        bottom: 0;
        width: 1280px;
        height: 14px;
        background-color:${boxTitleBg};
        border-radius: 0 4px 4px 8px;
    }
`;

const DivWithoutBefore = styled.div`
    min-height: 80px;
    background-color: ${boxTitleBg};
    border-radius: 8px;
    display: inline-block;
    margin-top: 10px;
    position: relative;
    font-family: 'grand_hotelregular';
    font-weight: bold;
    margin: 10px 0px;
`;

 

const DivTitle = styled.h1`
    font-size: 40px;
    align-self: center;
    color: white;
    padding: 0 22px;
    line-height: 80px;
`;

const TitleBox = (dados) => {
    const {text} = dados;
    return( 
        <Div>
            <DivTitle>{text}</DivTitle>
        </Div> 
    );
}

export const SubtitleBox = (dados) => {
    const {text} = dados;
    return( 
        <DivWithoutBefore>
            <DivTitle>{text}</DivTitle>
        </DivWithoutBefore> 
    );
}


export const SubTitleBox = () => {
    return(
        <Div>
           
        </Div> 
    );
}




export default TitleBox;