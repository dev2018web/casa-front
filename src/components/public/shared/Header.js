import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom'
import { Redirect } from 'react-router-dom'

const greenSubMenu = "#13CE66";
const Nav = styled.nav`
    max-width: 100%;
    min-height: 97px;
    background: #0D1926;
    display: flex;
    justify-content: space-between;
    position: relative;
    // z-index: 3; 

    ::after{
        content: '';
        position: absolute;
        min-height: 266px;
        width: 100%;
        bottom: -266px;
        background: white;
        z-index: 2;
    }
`;

const linkMenu = {
    'listStyle': 'none',
    'color': 'white',
    'textDecoration': 'none',
    'fontFamily': 'Lato, Arial'
}
const NavItens = styled.ul`
    align-self: center;
    padding-right: 80px;
`;

const ActionsItens = styled.ul`
    position: relative;
    min-height: 100%;
    z-index: 2;
    display: flex;
    padding-left: 80px;

    ::before {
        z-index: -1;
        content: '';
        position: absolute;
        background: #1B2A50;
        bottom: 0;
        top: -86px;
        left: -72px;
        right: 0;
        transform: rotate(120deg);
        width: 400px;
        height: 336px;
    }

    ::after{
        content: '';
    }
`;

const ItensItem = styled.li`
  align-self: center;
`;

const ItemLink = styled.a`
    color: white;
    margin: 0 20px;
    font-size: 18px;
`;

const isLogged = () => {
    if(!sessionStorage.getItem("token")){
        return false;
    }else{
        return true;
    }
}

const logout = () => {
    sessionStorage.removeItem("token");

    // return <Redirect to='/login' />

    window.location.href = "/login";
}

const Menu = () => {

    return(
        <Nav>
            <ActionsItens>
                <ItensItem>
                    { (!isLogged()) ?
                        <ItemLink><Link style={linkMenu} to="/login">Login</Link></ItemLink>

                    :   
                        <div>
                            <ItemLink><Link style={linkMenu} onClick={logout}>Logout</Link></ItemLink>
                            <ItemLink><Link style={linkMenu} to="/escolhas">Escolhas</Link></ItemLink>
                        </div>
                    }
                </ItensItem>
            </ActionsItens>
            <NavItens>
                { (isLogged()) && 
                <ItensItem>
                    <ItemLink><Link style={linkMenu} to="/home">Home</Link></ItemLink>
                    <ItemLink><Link style={linkMenu} to="/livros">Livros</Link></ItemLink>
                    <ItemLink><Link style={linkMenu} to="/sobre">Sobre Nós</Link></ItemLink>
                </ItensItem>
                }
            </NavItens>
        </Nav> 
    );
}


const BoxContainer = styled.div`
    position: relative;
    float: left;
    position: -webkit-sticky; /* Safari */
    position: sticky;
    top: 0;
    min-height: 100vh;
    background: ${greenSubMenu};
    width: 100%; 
    display: flex;
    align-content: center;
    justify-content: center;
    flex-direction: column;
    padding: 0 20px;

`;
const List = styled.ul`
    margin-bottom: 20px;
`;
const ListItem = styled.li`
    font-family: 'Lato', 'Arial';
    font-size: 20px;
`;

const BoxFilter = styled.div`
    margin: 0 auto;
    // padding: 20px 20px;
`;
const FilterTitle = styled.h1`
    font-size: 20px;
    color: white;
    font-weight: bold;
    font-family: 'Lato', 'Arial';
`;
const FilterForm = styled.form``;
const FormInput = styled.input`
    width: 100%;
    margin: 5px 20px 5px 0;
    min-height: 50px;
    font-size: 17px;
    height: 45px;
`;
const FormSelect = styled.select`
    width: 100%;
    margin: 5px 0;
    min-height: 50px;
    font-size: 17px;
    height: 45px;
`;
const SelectOption = styled.option``;

const Div = styled.div`

`;
export const BoxSubMenu = () => {
    return(
        <BoxContainer>
            <List>
                <ListItem><Link style={linkMenu} to="/livros">Livros</Link></ListItem>
                <ListItem><Link style={linkMenu} to="/livros-pegos">Livros Pegos</Link></ListItem>
                <ListItem><Link style={linkMenu} to="/doar-livro">Doar Livros</Link></ListItem>
                <ListItem><Link style={linkMenu} to="/livros-doados">Livros doados</Link></ListItem>
            </List>

            <BoxFilter>
                <FilterTitle>Filtro Avançado</FilterTitle>

                <FilterForm>
                    <FormInput type="text" name="q"/>
                    <FormSelect>
                        <SelectOption>Idioma</SelectOption>
                    </FormSelect>

                    <FormSelect>
                        <SelectOption>Estado</SelectOption>
                    </FormSelect>

                    <FormSelect>
                        <SelectOption>Gênero</SelectOption>
                    </FormSelect>
                </FilterForm>
            </BoxFilter>
        </BoxContainer>
    )
}


const Box = styled.div`
    width: 100%;
    min-height: 580px;
    background: url('https://miro.medium.com/max/3760/1*HoT1qwwVIKjHI6Sjzcj7gg.jpeg');
    background-position: center;
    position: relative;
    z-index: 2;
`;


export const Slider = () => {
    return(
        <Box>

        </Box>
    );
}

export default Menu;