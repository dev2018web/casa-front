import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import {Button, TextButton, TextButtonAddBook} from './Buttons';
import { Row, Col } from 'react-bootstrap';

// COLORS
const blackTitle = "#000";
const branco = "#fff";

const Box = styled.div`
    padding-top: 20px;
    display: flex;
    flex-wrap: wrap;

`;
const BoxImage = styled.div`
    width: 100%;
    min-height: 300px;
    background: url('${props => props.img || 'https://via.placeholder.com/300'}');
    background-size: cover;
    background-position: center;
`;

const BoxItem = styled.div`
    width: 100%; 
`;

const colorText = "#A30051";

const Margin = styled.div`
    margin: 10px 20px;
`;

export const Paragrafo = styled.p`
    margin: 10px 0;
    font-size: 20px;
    font-family: 'Lato', sans-serif;
    color: black;
    text-decoration: none;

`;

const BoxContent = styled.p`
    min-height: 130px;
    font-family: 'Lato', sans-serif;
    text-decoration: none;

`;

export const Container = styled.div`
  max-width: 90%;
  margin: auto;
  position: relative;
  margin: auto;
  z-index: 2;
`;
export const ContainerFluid = styled.div`
    width: 100%;
    position: relative;
    z-index: 2; 
`;
export const SubContainer = styled.div`
    padding: 0 0px;
`;


const linkStyled = {
    'text-decoration': 'none'
}
const BoxContentTitle = styled.h1`
    font-size: 20px;
    text-transform: capitalize;
    color: ${blackTitle};
    margin: 10px 0;
`;

const Product = (props) => {
    let { books, setBookId, addBook } = props.data;

    return(
        <Box>
            <Row>
                {(books) ? books.map((book) => 
                    <Col sm={12} md={6} lg={3}>
                        <BoxItem>               
                            <Link onClick={() => setBookId()} to={`/livro/${book.id}`} style={linkStyled}>
                                <Margin>
                                    <BoxImage img={book.volumeInfo.imageLinks.thumbnail}/>
                                        <BoxContent>        
                                            <BoxContentTitle>{book.volumeInfo.title}</BoxContentTitle>
                                            {(book.volumeInfo.description) ? <Paragrafo>{console.log(), book.volumeInfo.description.substring(0,100)}...</Paragrafo> : ''}
                                        </BoxContent>
                                    <Button onClick={() => addBook(book)}><TextButtonAddBook>Adicionar</TextButtonAddBook></Button>
                                </Margin>
                            </Link>
                        </BoxItem>
                    </Col>
                ) : <h1>Carregando...</h1> }
            </Row>
        </Box>        
    );
}


const IsBookChosed = (item, livros) => {
    (livros) && livros.map(livro => {
        // return livro.id == item.id;
        // console.log(`${livro.id}==${item.id} = ${livro.id = item.id}`);
    })
}


export const BooksChosenArea = (props) => {
    let { booksNotChosed,booksChosed, setBookId, addBook } = props.data;

    return(
        <Box>
            <Row>
            {(booksNotChosed) ? booksNotChosed.map((book) => 
                 <Col sm={12} md={6} lg={3}>
                    <BoxItem>               
                            <Margin>
                                <BoxImage img={book.volumeInfo.imageLinks.thumbnail}/>
                                    <BoxContent>        
                                        <BoxContentTitle>{book.volumeInfo.title}</BoxContentTitle>
                                        {(book.volumeInfo.description) ? <Paragrafo>{book.volumeInfo.description.substring(0,100)}...</Paragrafo> : ''}
                                    </BoxContent>
                                {(1 == 1) ? ( 
                                <Button onClick={() => addBook(book)}><TextButtonAddBook>Adicionar</TextButtonAddBook></Button>
                                ) : 'Sem bottão'}
                            </Margin>
                    </BoxItem>
                    </Col>
            ) : <h1>Carregando...</h1> }
            </Row>
        </Box>        
    );
}

export const Book = styled.div`
    display: flex;
    margin-top: 20px;
`;

export const BookContent = styled.div`
    flex-shrink: 2;
    margin-left: 20px;
`;
export const ContentTitle = styled.h1`
    font-size: 30px;
    font-weight: bold;
    text-transform: capitalize;
    color: #47525E;
    font-family: 'Lato', sans-serif;
`;
export const ContentSubTitle = styled.h2`
    font-size: 20px;
    font-weight: bold;
    color: #47525E;
    font-family: 'Lato', sans-serif;
    margin: 10px 0;
    text-transform: capitalize;
`;
export const Lorem = styled.p`
    margin: 10px 0;
    font-family: 'Lato', sans-serif;
    ::before {
    content: 'Define Styled Components outside of the render method
    It is important to define your styled components outside of the render method, otherwise it will be recreated on every single render pass. Defining a styled component within the render method will thwart caching and drastically slow down rendering speed, and should be avoided.
    Write your styled components the recommended w';
    }
`;

const btnLefted = {
    'max-width': '280px'
}


export const BookDetailed = (dados) => {
    const { data } = dados;
    return(
        <div>
            {(dados.data) ? 
            <Book>
                
                <BoxImage img={(data.imageLinks) ? data.imageLinks.extraLarge : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAACoCAMAAABt9SM9AAAAS1BMVEX///+hoaGjo6Onp6f39/ft7e2+vr6ysrL8/Pz09PTGxsalpaWqqqqvr6/i4uLz8/PX19fKysrn5+fAwMC4uLje3t7U1NTJycnQ0NB9QKqOAAAEuklEQVR4nO3Z6ZKrKhQG0DCJMjlhmvd/0oNxAo2ddNe9J56qb/3oijhmBzZb+nYDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD475Sc8/L1Ye8ddXL9dy73/LhLEcoyzWxx3z9ooZKNcvBmPGr44dXLTnnDmJFtnTbXrYw3NT5srfX4HEZ+1YdrXEapiAxd5xrFmnyP0nLbqCXpm65rPPE/+vGdJUUYum5ojb5vzXdtWxdv2tr1B7lTG2JTYPvnuBBPl2cr8yg4ardgccvE0ixvP6DCctX4q6y9UpFQ7m4aiJqayoL8tPf+LS3tnu/gOqgtLD0Ry0dHwi/v5dn8YSCHziN0nxx3zcwljo89k/ZWrMESNAmQYtXvbjaQKR2VVB329UmAKvr1uxv8z5Q9md8CrZJgtTqJz3mAX3Bz97zTQ7QrlsanP3uqjyrp/fmOjsa8sQXL+3Sn9Pmx2ZY4H0INmWJg+8MuR9KrDOQkN3yUo+JpOzfjQFmDFfNXulfZNCA1aZMtoY9DbOGn69XaHXa1WZqqyMlv+FHBxN+6br76PnRpz5/GwRosQbKv19CsFBroFi2hzysLNZ/mSByFfGj7vnXLwYVPb1/a84h/TgxHVVDbq94Su3X9Zupwa7AcyTrgkG/G7SXhCCZPsk3ppJnPCiSWEdoUSkli5vQni+zgfPMipKq1n76DkGverufiYA3Writ1+5QykKknCGaf3KRpg/LMb0WWvRkzXaHqpzG870p5jrwIU5htCBVLhzHzo54Fazcqb0u0hH5arxYmvu6wvln6XOGTGaJ9VKD7YBVXDBZjSX8v5yC1es4k7wfrES3B/OmMz8XdLjOvZybZ48e0uQ9WfzaaP4mxNPsEPcZkm8XfHoa3Mcv35jxWD/d5dHud1mlujPw/MQxZVv2JcUhwtj73Gqyl9J65Z2VQ0PbVS0ogj2LU0/RAPiaCUuall79igjdZkuFjebNVotvHXVdqyHEVJc6D7NVLylyuFSRtLB8lap6kyidl6+fJQ7Du2omFt3X8W44va1mR2JpDH4r1VaxwX1VH5tFhFMlrqjEwKrskP3ux+Chl8mHYxDFCyYKx8a84/NLHjDLNg0sFcWqaQfKOOb0s5G3dcQa5gCFboHFxi1crXsi4VY/hzILKD2s0gk7hc+T74TOlQ0HTBC8e6b5OVwZvX8euewFl9iZXmHw629KXS9fjgt69UI5jcDnuu761RMmmpWuY+pRP2ri+Ysoal463Ly72PSbJ9X6LY6V3U1WM1brz22j18zR4T0JfTXksnrh1t6+zBckP48Yu0RJmX38nwdoCwi3L58KapvWVy9Yg4mSw7WvXtQTPlpxUSzOvbfV0iWBDrrn2N4aItYJz3iktD//dSaI3UDPUnNeNYftVnSYbvC7frU3v4nlcNHLruFzSfrypCGx5u76VnqjYVnX9q2nig3hgNL66re//iTSP3OqCaMYoVT9bUy4bP57HiC7SKDaGjI0s8EObveq/KybCue6NEJSdc8/XCl9f/3hiHRv3te3YdsV5EAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+qj8dkCpgHw38BwAAAABJRU5ErkJggg=='}/>

                <BookContent>
                <ContentTitle>{data.title}</ContentTitle>
                
                <ContentSubTitle>Autor: 
                {(data.authors) ? data.authors.map((author) =>
                    <span>{author}</span>
                ) : "desconhecido" }
                </ContentSubTitle>

                <Paragrafo>Estado do Livro</Paragrafo>
                <Paragrafo>Quantidade: 10</Paragrafo>
                <Paragrafo>Idioma: português</Paragrafo>
                <Paragrafo>Editora: {data.publisher}</Paragrafo>

                <Lorem>{data.description}</Lorem>

                <Button style={btnLefted}><TextButtonAddBook>Adicionar</TextButtonAddBook></Button>
                </BookContent>
            </Book>
            : 'Carregando'}
        </div>
    );
}


// CARDS HORIZONTAIS
const ContainerHorizontal = styled.div`
    padding-top: 10px;
    display: flex;
    flex-wrap: wrap;
    width: 100%;
    padding-bottom: 20px;
`;
const DivHorizontal = styled.div`
    display: flex;
    margin: 10px 0 0 0px;
    min-height: 145px;
    background-color: #EEEEEE;
    border-radius: 8px;
    font-family: Roboto;
    position: relative;
    width: 100%;
    justify-content: space-between;
`;
const CardImage = styled.div`
    margin: 10px 15px 10px 25px;
    width: 140px;
    min-height: 100px;
    background: url('${props => props.img || 'https://via.placeholder.com/300'}');
    background-size: cover;
    background-position: center;
`;
const CardContent = styled.div`
    align-self: center;
    width: 100%;
`;
const HorizontalTitle = styled.h1`
    font-size: 22px;
    text-transform: capitalize;
    margin-bottom: 10px;
`;
const HorizontalButton = styled(Button)`
    align-self: center;
    width: 100px;
    background: #13CE66;
    border: none;
    border-radius: 8px;
    margin: 10px;
    width: 200px;
`;
const TextButtonHorizontal = styled(TextButton)`
    color: ${branco};
`;

export const HorizontalBoxes = (dados) => {
    const {data} = dados;

    return(
        <ContainerHorizontal>
        {(data) ? data.map((book) => 
            <DivHorizontal>
                <CardImage img={(book.volumeInfo.imageLinks) ? book.volumeInfo.imageLinks.smallThumbnail: ''}/>
    
                <CardContent>
                    <HorizontalTitle>{book.volumeInfo.title.substring(0,40)}</HorizontalTitle>
                    
                    <p>Lorem ipsum dolor sit amet consectetur...</p>
                </CardContent>

                <HorizontalButton><TextButtonHorizontal>20/10/2019</TextButtonHorizontal></HorizontalButton>
            </DivHorizontal>
        ) : <h1>Carregando...</h1> }
        </ContainerHorizontal>
    );
}


const HorizontalRemoveBookButton = styled(Button)`
    align-self: end;
    // align-content: center;
    // justify-content: center;
    border-radius: 50%;
    height: 40px;
    width: 60px;
    line-height: 35px;
    border: 3px solid black;
    margin: 10px;
    font-weight: bold;
`;

export const ChosedBooksBox  = (dados) => {
    const {data, deleteBook} = dados;

    return(
        <ContainerHorizontal>
        {(data) ? data.map((book) => 
            <DivHorizontal>
                <CardImage img={(book.volumeInfo.imageLinks) ? book.volumeInfo.imageLinks.smallThumbnail: ''}/>
    
                <CardContent>
                    <HorizontalTitle>{book.volumeInfo.title.substring(0,40)}</HorizontalTitle>
                    
                    <p>Lorem ipsum dolor sit amet consectetur...</p>
                </CardContent>

                <HorizontalRemoveBookButton onClick={() => deleteBook(book.id)}>X</HorizontalRemoveBookButton>
            </DivHorizontal>
        ) : <h1>Carregando...</h1> }
        </ContainerHorizontal>
    );
}

export default Product;