import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom'
import {ButtonProsseguir, TextButton, BtnProsseguirEndereco} from './Buttons';
import {Row, Col, Alert} from 'react-bootstrap';

const colorTextInput = "#47525E";

const Form = styled.form`
    // padding: 0 10px;
`;

export const Input = styled.input`
    min-height: 55px;
    margin: 0;
`;

const Label = styled.label`
    font-size: 20px;
    font-weight: bold;
    font-family: 'Lato', 'Arial';
`;

const Select = styled.select`
`;

const Option = styled.option`
`;

const TextArea = styled.textarea`
`;

export const FormDonateBooksData = () => {
    return(
        <Form>
            <Input type="text" name="name" placeholder="Quantidade de livros"/>
            
            <Label>Gênero do Livro: 
                <Select>    
                    <Option>Romance</Option>
                </Select>
            </Label>

            <Label>Estado do Livro:
                <Select>
                    <Option>Novo</Option>
                </Select>
            </Label>

            <Label>Outros detalhes:
                <TextArea/>
            </Label>

            <ButtonProsseguir><TextButton>Prosseguir</TextButton></ButtonProsseguir>
        </Form>
    )
}


const InputInfo = styled(Input)`
    height: 50px;
    width: 100%;
    max-width: 100%;
    margin-bottom: 20px;
    margin-top: 10px;
    font-size: 20px;
    font-family: 'Lato', 'Arial';
    color: ${colorTextInput};
`;

const TitleForm = styled(Label)`
    font-size: 40px;
    margin-bottom: 20px;
`;

export class FormInformations extends React.Component {
    state = {
        data: {
            name: '',
            sobrenome: '',
            cpf: '',
            rg: '',
            email: '',
            telefone: '',
            cep: '',
            cidade: '',
            bairro: '',
            rua: '',
            numero: '',
        },
        classe: this.props.classe,
        alertError: false
    }

    handleForm = () => {
       const {data} = this.state;
       const {classe} = this.state;

       var stop = false;

       Object.keys(data).map(input => {
           if(!data[input] && !stop){
                this.setState({message: `Você precisa preencher o campo de ${input}`, alertError: true})

                stop = true;
           }
       });

       if(!stop){
           this.setState({alertError: false})

           classe.props.history.push("/emprestimo-solicitado");
       }
    }

    handleInput = (event) => {
        const {name} = event.target;
        const {value} = event.target;
        const {data} = this.state;

        this.setState({data: {...data, [name]: value}})
    }

    render(){
        const {message, alertError} = this.state;

        return(
            <Row>
                <Col>
                    <Form style={{marginTop: '50px'}}>
                        <Row>
                            <Col>
                                <Label>Nome:</Label> 
                                <InputInfo onChange={this.handleInput} type="text"  name="name" placeholder="Seu nome"/>
                            </Col>
                            <Col>
                                <Label>Sobrenome:</Label> 
                                <InputInfo onChange={this.handleInput} type="text" name="sobrenome" placeholder="Seu sobrenome"/>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Label>CPF:</Label>
                                <InputInfo onChange={this.handleInput} type="text" name="cpf" placeholder="Seu CPF"/>

                            </Col>
                            <Col>
                                <Label>RG:</Label> 
                                <InputInfo onChange={this.handleInput} type="text" name="rg" placeholder="Seu RG"/>
                            </Col>
                        </Row>

                        <Row>
                            <Col>
                                <Label>E-mail:</Label> 
                                <InputInfo onChange={this.handleInput} type="email" name="email" placeholder="Seu e-mail"/>
                            </Col>
                            <Col>
                                <Label>Telefone:</Label> 
                                <InputInfo onChange={this.handleInput} type="text" name="telefone" placeholder="Seu Telefone"/>
                            </Col>
                        </Row>

                        <TitleForm>Endereço</TitleForm>

                        <Row>
                            <Col>
                                <Label>CEP:</Label> 
                                <InputInfo onChange={this.handleInput} type="text" name="cep" placeholder="Seu CEP"/>
                            </Col>
                            <Col>
                                <Label>Cidade:</Label> 
                                <InputInfo onChange={this.handleInput} type="text" name="cidade" placeholder="sua cidade"/>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Label>Bairro: </Label> 
                                <InputInfo onChange={this.handleInput} type="text" name="bairro" placeholder="Seu Bairro"/>
                            </Col>
                            <Col>
                                <Label>Rua</Label> 
                                <InputInfo onChange={this.handleInput} type="text" name="rua" placeholder="Rua"/>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Label>Número:</Label> 
                                <InputInfo onChange={this.handleInput} type="text" name="numero" placeholder="Nº da casa"/>
                            </Col>
                            <Col>
                                <Label>Complemento (Não obrigatório):</Label> 
                                <InputInfo type="text" name="complemento" placeholder="Complemento"/>
                            </Col>
                        </Row>
                    </Form>
                </Col>
                <Col style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between', marginTop: '80px'}}>
                    <div>
                        <Alert show={alertError} variant="danger">
                                <p>{message}</p>
                        </Alert>
                    </div>
                    

                    <BtnProsseguirEndereco onClick={() => this.handleForm()} className={'align-self-start'}><TextButton>Prosseguir</TextButton></BtnProsseguirEndereco>
                </Col>
            </Row>
        )
    }
}
