import React from 'react';
import { Slider } from './shared/Header';
import Product from './shared/Product';
import axios from 'axios';
import styled from 'styled-components';
import TitleBox from './shared/BoxesTitles';
import { restricted } from './configs/restrictedArea';

export const Container = styled.div`
  max-width: 1280px;
  margin: auto;
  position: relative;
  z-index: 2;
  overflow: hidden;
`;


class Home extends React.Component {
    constructor(props){
      super(props);
      this.getBooks();

      this.state = {
        books: []
      }
    }

    getBooks = () => {
      axios.get(`https://www.googleapis.com/books/v1/volumes?q=software`)
      .then(res => {
        const { items } = res.data;

        this.setState({ books: items });
      })
    }

    setBookId = () =>{

    }
    render() {
      const { books } = this.state;
      restricted(this);

      return (

        <div>
            <Slider/>


            <Container>
            <TitleBox text={"Novidades"}/>

              <Product data={{books, setBookId: this.setBookId}}/>
            </Container>
        </div>
      );
    }
  }


export default Home;