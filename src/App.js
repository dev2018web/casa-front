import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";


import Login from './components/public/Login';
import Menu from './components/public/shared/Header';
import Home from './components/public/Home';
import Books from './components/public/Books';
import BookDetail from './components/public/BookDetail';
import TakenBooks from './components/public/TakenBooks';
import DonateBooks from './components/public/DonateBooks';
import DonatedBooks from './components/public/DonatedBooks';
import ChosenBooks from './components/public/ChosenBooks';

import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/teste.css';

import { AnimatedSwitch } from 'react-router-transition';
import FillAddressEndereco from './components/public/FillAddressEndereco';
import EmprestimoSolicitado from './components/public/EmprestimoSolicitado';

function App() {
  return (
    <div>
    <Router>
      <Menu/>

      <AnimatedSwitch>
          <Route exact path="/home" component={Home}/>
          <Route exact path="/livros" component={Books}/>
          <Route exact path="/livros-pegos" component={TakenBooks}/>
          <Route exact path="/doar-livro" component={DonateBooks}/>
          <Route exact path="/livros-doados" component={DonatedBooks}/>
          <Route exact path="/escolhas" component={ChosenBooks}/>
          <Route path="/solicitacao-emprestimo" component={FillAddressEndereco}/>
          <Route path="/emprestimo-solicitado" component={EmprestimoSolicitado}/>
          <Route path="/livro/:livro_id" component={BookDetail}/>
          <Route any path="" component={Login}/>

        {/* <Footer/> */}
      </AnimatedSwitch>
    </Router>
    </div>

  );
}

export default App;
